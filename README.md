# `pg_readable_conf`
`pg_readable_conf` is a simple tool to make a `postgresql.conf` file more human
readable. A lot of people use Postgres sample conf file to set their PostgreSQL
configuration.

This file is really great to learn, because there are so many comments to
explain each setting and what kind of value you should use, but it's not great
for a day to day production job.

`pg_readable_conf` will strip all the comments off the file, leaving only the
necessary lines (as comments that devides the file in sections and actual
configuration settings lines).

By default, `pg_readable_conf` will use standard input and output. Logs will be
sent to `pg_readable_conf.log`.

Use the `--help` switch to see more options.

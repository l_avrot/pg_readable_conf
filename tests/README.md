# How to test
You'll find a "dirty" `postgresql.conf` file in that directory and a
`wanted_postgresql.conf` file. You'll also find a `minimum_postgresql.conf` file.

## Using wanted\_postgresql.conf file
Just launch `pg_readable_conf` with the `postgresql.conf` file you'll find in that
directory and perform a `diff` between your result file and
`wanted_postgresql.conf` file you'll find here.

## Using minimum\_postgresql.conf
If you want to be sure this tool won't alter your settings by removing too many
lines, you may use the `minimum_postgresql.conf` file.

Just launch `pg_readable_conf` with the postgresql.conf file you'll find in that
directory and perform this command on the result file:

    grep -v '^\s*#' path_to_your_file | grep -v '^$' | diff path_to_the_minimum_file -
